#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys, site
import os

# Add the site-packages of the chosen virtualenv to work with
#site.addsitedir('//home/mash/data/projects/tylipa/dev/tylipa/api/venv/lib/python3.5/site-packages')

#activate_this = '/home/mash/data/projects/tylipa/dev/tylipa/api/venv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))

# for python 3
activate_this = '/home/mash/data/projects/tylipa/dev/tylipa/api/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))


sys.path.insert(0, '/home/mash/data/projects/tylipa/dev/tylipa/api/')
from thisapp import app as myapp
