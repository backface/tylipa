#!/usr/bin/python3
from wsgiref.handlers import CGIHandler

#activate_this = '/home/mash/data/projects/tylipa/dev/tylipa/api/venv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))

# for python 3
activate_this = '/home/mash/data/projects/tylipa/dev/tylipa/api/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

from thisapp import app

CGIHandler().run(app)
