#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import Flask
from flask import jsonify, request, Response
import os
import json
from web3 import Web3

NETWORK = "mainnet"
NETWORK_ID = "1"
INFURA_ID = "6f38a3475ff7449a8e2a2632e7285501"
CONTRACT_FILE = "contracts/Tylipa.json"


app = Flask(__name__)


def get_img(id):
    w3 = Web3(
        Web3.HTTPProvider("https://{}.infura.io/v3/{}".format(
            NETWORK, INFURA_ID)))

    with open(CONTRACT_FILE,  encoding='utf-8') as f:
        info_json = json.load(f)
		
    try:
        abi = info_json["abi"]
        address = info_json["networks"][NETWORK_ID]["address"]       
        contract = w3.eth.contract(address=address, abi=abi)
        img_data = contract.functions.tokenImg(id).call()
        img_data = img_data.encode('utf-8').decode('unicode_escape')
        return img_data
    except Exception as err:
        return "err"


def get_meta(id):
    w3 = Web3(
        Web3.HTTPProvider("https://{}.infura.io/v3/{}".format(
            NETWORK, INFURA_ID)))

    with open(CONTRACT_FILE,  encoding='utf-8') as f:
        info_json = json.load(f)

    try:
        abi = info_json["abi"]
        address = info_json["networks"][NETWORK_ID]["address"]
        return address
        contract = w3.eth.contract(address=address, abi=abi)
        img_data = contract.functions.tokenMetadata(id).call()
        img_data = img_data.encode('utf-8').decode('unicode_escape')
        return img_data
    except Exception as err:
        return "err"



@app.route('/')
def index():
        return "no"


@app.route('/flower/<id>')
def token(id):
    id = int(id)
    img_file = "images/{}.svg".format(id)

    if not os.path.isfile(img_file):
        img_data = get_img(id)
        if len(img_data) > 100:
            with open(img_file, "w") as img:
                img.write(img_data)

    return jsonify({
		'image': "https://{}/image/{}.svg".format(request.host, id),
        'name': "ŦɏŁᵻⱣⱥ Flower #{}".format(id),
        'description':
            "ŦɏŁᵻⱣⱥ is autonomous code that generates art on the Ethereum blockchain. " \
            "Any wallet address can grow a limited number of unique flower species. " \
            "ŦɏŁᵻⱣⱥ the tulpenmanie for the Dark Ages of the 21st century!",        
        "external_url": "https://www.tylipa.art"
    })


@app.route('/cached/<id>')
def cached(id):
    id = int(id)
    img_file = "images/{}.svg".format(id)

    if not os.path.isfile(img_file):
        img_data = get_img(id)
        if len(img_data) > 100:
            with open(img_file, "w") as img:
                img.write(img_data)
    else:
        with open(img_file) as img:
            img_data = img.read()
    return Response(img_data, mimetype='image/svg')

@app.route('/image/<id>')
def direct(id):
    id = int(id)
    return Response(get_img(id) or "" , mimetype='image/svg')
    
@app.route('/image/<id>.svg')
def directSVG(id):
    id = int(id)
    return Response(get_img(id) or "" , mimetype='image/svg')
    
@app.route('/meta/<id>')
def getMetafromBlockchain(id):
    id = int(id)
    return Response(get_meta(id) or "" , mimetype='application/json')



if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)
