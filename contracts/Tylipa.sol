pragma solidity ^0.5.0;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Full.sol";
import "./Strings.sol";

/**
 * @title ŦɏŁᵻⱣⱥ
 * Tylipa - a contract for non-fungible flowers / generates on-chain art.
 * When the flowers start to die outdoors, we grow them on the blockchain.
 * ŦɏŁᵻⱣⱥ is the tulpenmanie for the Dark Ages of the 21st century.
 * Clearly this is what the blockchain was invented for!
 */

contract OwnableDelegateProxy { }

contract ProxyRegistry {
    mapping(address => OwnableDelegateProxy) public proxies;
}

contract Tylipa is ERC721Full, Ownable {

  // Mapping from token ID to grower
  mapping (uint256 => address) private _tokenGrower;

  // Mapping from birth timestamp to tokenID
  mapping(uint256 => uint256) private _tokenBirth;

  // Mapping from birth timestamp to tokenID
  mapping(address => uint256) private _lastGrown;

  uint256 private _currentTokenId = 0;

  // has a minting fee
  uint256 public mintingFee = 0.2 ether;

  // donate to 350.org (https://350.org/other-ways-to-give/)
  address payable beneficiary = address(0x50990F09d4f0cb864b8e046e7edC749dE410916b);
  // distribute 50%
  uint public beneficiaryWeight = 50;

  // grow interval
  uint256 public growInterval = 7 days;

  address proxyRegistryAddress;

  // make payable
  function() external payable {}

  // constructor
  constructor(address _proxyRegistryAddress) ERC721Full("ŦɏŁᵻⱣⱥ", "ŦLⱣ") public {}


  //  public functions

  function baseTokenURI() public pure returns (string memory) {
    return "https://api.tylipa.art/flower/";
  }

  function tokenURI(uint256 _tokenId) external view returns (string memory) {
    return Strings.strConcat(baseTokenURI(), Strings.uint2str(_tokenId));
  }
  
  /**
   * @dev Gets the grower of the specified token ID
   * @param tokenId uint256 ID of the token to query the grower of
   * @return grower address if grower currently marked as the grower of the given token ID
   */
  function growerOf(uint256 tokenId) public view returns (address) {
    address grower = _tokenGrower[tokenId];
    require(grower != address(0));
    return grower;
  }

  /**
   * @dev gets the minting feet
   * @return minting fee
   */
  function getMintingFee() public view returns (uint256) {
      return mintingFee;
  }

  /**
   * @dev gets the beneficiaryWeight address
   * @return beneficiary address
   */
  function getBeneficiary() public view returns (address) {
      return beneficiary;
  }

  /**
   * @dev can address mint a token?
   * @param grower / address of the potential grower
   * @return bool can mint or not
   */
  function canMint(address grower) public view returns (bool) {
    if (block.timestamp >= _lastGrown[grower] + growInterval) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @dev get time to wait until allowed to grow (again)
   * @param grower / address of the potential grower
   * @return timediff in seconds
   */
  function mintWaitTime(address grower) public view returns (uint256) {
    if (block.timestamp >= _lastGrown[grower] + growInterval) {
      return 0;
    } else {
      return (_lastGrown[grower] + growInterval) - block.timestamp;
    }
  }

  /**
   * @dev mint a token
   */
  function mint() payable public returns (uint256) {
    require(canMint(msg.sender), "Growing takes patience");
    address payable owner = address(uint160(owner()));
    uint256 tokenId = _getNextTokenId();

    if(!isOwner()) {
      require(msg.value >= mintingFee, "Insufficient ETH sent with transaction");
      require(!_exists(tokenId), "Token ID already exists");

      if (beneficiary != address(0)) {
        beneficiary.transfer(mintingFee.mul(beneficiaryWeight).div(100));
        owner.transfer(mintingFee.mul((100-beneficiaryWeight)).div(100));
      } else {
        owner.transfer(mintingFee);
      }
    }
    super._mint(msg.sender, tokenId);
    _incrementTokenId();
    _tokenGrower[tokenId] = msg.sender;
    _tokenBirth[tokenId] = now;
    _lastGrown[msg.sender] = now;
  }

  /**
   * @dev Mints a token to an address with a tokenURI.
   * @param to address of the future owner of the token
   */
  function mintTo(address to) public onlyOwner {
    uint256 tokenId = _getNextTokenId();
    super._mint(to, tokenId);
    _incrementTokenId();
    _tokenGrower[tokenId] = msg.sender;
    _tokenBirth[tokenId] = block.timestamp;
    _lastGrown[msg.sender] = block.timestamp;
  }

  /**
   * @dev returns the "DNA" of a token to generate the image
   * @param tokenId of flower to get dna for
   * @return address with dna
   */
  function tokenDNA(uint256 tokenId) public view returns (address) {
    require(_exists(tokenId));
    bytes32 hash = keccak256(abi.encodePacked(_tokenGrower[tokenId],_tokenBirth[tokenId]));
    return address(uint256(hash));
  }

  /**
   * @dev math helper for max
   */
  function max(uint a, uint b) private pure returns (uint) {
    return a > b ? a : b;
  }

  /**
   * @dev returns image for token id
   * @param tokenId of flower generate image for
   * @return a svg string
   */
  function tokenImg(uint256 tokenId) public view returns (string memory) {
    require(_exists(tokenId));
    return address2SVG(tokenDNA(tokenId));
  }

  /**
   * @dev returns image for an address
   * @param _address - any address
   * @return a svg string
   */
  function address2SVG(address _address) public pure returns (string memory) {
    bytes20 src = bytes20(_address);
    bytes memory s = '<svg width="320" height="320" version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg">';
    //s = abi.encodePacked(s,"<style>ellipse {opacity:0.3}</style>");

    uint256 pointer = 0;
    uint256 last;

    for (uint256 i = 0; i < 4; i++) {
      uint256 x = uint8((src[pointer] >> 4) & 0x0f);
      uint256 y = uint8(src[pointer++] & 0x0f);
      uint256 n = uint8((src[pointer] >> 4) & 0x0f);
      uint256 r = uint8(src[pointer++] & 0x0f);
      uint256 g = uint8((src[pointer] >> 4) & 0x0f);
      uint256 b = uint8(src[pointer++] & 0x0f);
      n = max(n,4);

      s = abi.encodePacked(s,"<g>");
      for (uint256 j = 0; j < n; j++) {
        s = abi.encodePacked(s,'<g transform="translate(160,160)">');
        s = abi.encodePacked(s,'<ellipse');
        s = abi.encodePacked(s,' transform="rotate(', Strings.uint2str((j % n) * (360/n)), ')"');
        s = abi.encodePacked(s,' cx="', Strings.uint2str(x*6), '"');
        s = abi.encodePacked(s,' rx="', Strings.uint2str((x+1)*3), '"');
        s = abi.encodePacked(s,' ry="', Strings.uint2str((y+1)*3), '"');
        s = abi.encodePacked(s,' fill="rgb(', Strings.uint2str(r*16), ',',
          Strings.uint2str(g * 10),',',
          Strings.uint2str(b * 10),')"'
        );
        s = abi.encodePacked(s,' style="opacity:0.3"');
        s = abi.encodePacked(s,' />');
        s = abi.encodePacked(s,'</g>');
      }
      s = abi.encodePacked(s,'</g>\n');
      last = b;
    }

    s = abi.encodePacked(s,'<circle cx="160" cy="160" style="opacity:0.7" fill="yellow" r="', Strings.uint2str(last), '" />');
    s = abi.encodePacked(s,'</svg>\n');
    return string(s);
  }

  /**
    * @dev gets all tokens of an _owner
    * @param _owner address
    * @return array of token IDs
    */
  function tokensOfOwner(address _owner) public view returns (uint256[] memory) {
    return super._tokensOfOwner(_owner);
  }


  // ownwer only functions

  /**
   * @dev sets the beneficiary address
   * @param _address of new beneficiary
   */
  function setBeneficiary(address _address) public onlyOwner {
    beneficiary = address(uint160(_address));
  }

  /**
   * @dev sets the beneficiary weight
   * @param weight of beneficiary
   */
  function setBeneficiaryWeight(uint256 weight) public onlyOwner {
    beneficiaryWeight = weight;
  }

  /**
   * @dev sets the minting feet
   * @param value of new minting fee
   */
  function SetMintingFee(uint256 value) public onlyOwner {
      mintingFee = value;
  }

  // private functions

  /**
   * @dev calculates the next token ID based on value of _currentTokenId
   * @return uint256 for the next token ID
   */
  function _getNextTokenId() private view returns (uint256) {
    return _currentTokenId.add(1);
  }

  /**
   * @dev increments the value of _currentTokenId
   */
  function _incrementTokenId() private  {
    _currentTokenId++;
  }

  /**
   * @dev Internal function to burn a specific token
   * Reverts if the token does not exist
   * @param tokenId uint256 ID of the token being burned
   */
  function _burn(uint256 tokenId) internal {
    super._burn(ownerOf(tokenId), tokenId);
    _tokenGrower[tokenId] = address(0);
  }


  // hacks
  
  function tokenMetadata(uint256 _tokenId) external pure returns (string memory) {    
    return string(abi.encodePacked(
		"{\"description\":\"ŦɏŁᵻⱣⱥ is autonomous code that generates flower",
		" art on the blockchain.\",\"external_url\":\"https://www.tylipa.art\",",
		//"\"image_data\":\"",  tokenImg(_tokenId), "\",",
		"\"name\":\"ŦɏŁᵻⱣⱥ Flower #", 
		Strings.uint2str(_tokenId),
		"\"}"
	));
  }  

  /**
   * Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-less listings.
   */
  function isApprovedForAll(
    address owner,
    address operator
  )
    public
    view
    returns (bool)
  {
    // Whitelist OpenSea proxy contract for easy trading.
    ProxyRegistry proxyRegistry = ProxyRegistry(proxyRegistryAddress);
    if (address(proxyRegistry.proxies(owner)) == operator) {
        return true;
    }

    return super.isApprovedForAll(owner, operator);
  }

}
