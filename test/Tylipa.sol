const Tylipa = artifacts.require("Tylipa");

contract("Tylipa", accounts => {
  it("...should be mintable.", async () => {
    const Tylipa = await Tylipa.deployed();

    // Set value of 89
    //await simpleStorageInstance.set(89, { from: accounts[0] });

    // Get stored value
    const canMint = await Tylipa.canMint().call();

    assert.equal(canMint, true, "can not mint");
  });
});
