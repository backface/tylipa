const path = require("path");

//require('dotenv').config();
const HDWalletProvider = require("truffle-hdwallet-provider");
const MNEMONIC = process.env.MNEMONIC
const INFURA_KEY = process.env.INFURA_KEY
//const MNEMONIC="tool admit problem census lake begin favorite olive bind around wolf effort"
//const INFURA_KEY ="6f38a3475ff7449a8e2a2632e7285501";

console.log(process.envMNEMONIC);

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    development: {
      host: "127.0.0.1",     // Localhost (default: none)
      port: 8545,            // Standard Ethereum port (default: none)
      network_id: "*",       // Any network (default: none)
      websockets: true
    },
    rinkeby: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://rinkeby.infura.io/v3/" + INFURA_KEY
        );
      },
      network_id: "*",
      skipDryRun: true,
      gasPrice: 5000000000, // 5 GWei
      //gas: 5000000
    },
    live: {
      network_id: 1,
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://mainnet.infura.io/v3/" + INFURA_KEY
        );
      },
      gas: 7000000,
      //sgasPrice: web3.utils.toWei('9', 'gwei'),
      gasPrice: 10000000000, // 7.5 GWei
      //gasPrice: 50000000000,
      skipDryRun: true,
  	},
  }
};
