const HDWalletProvider = require("truffle-hdwallet-provider")
const web3 = require('web3')
const getEnv = env => {
  const value = process.env[env];  if (typeof value === 'undefined') {
    throw new Error(`${env} has not been set.`);
  }  return value;
};

console.log(getEnv("INFURA_KEY"));
const MNEMONIC = process.env.MNEMONIC
const INFURA_KEY = process.env.INFURA_KEY
const OWNER_ADDRESS = process.env.OWNER_ADDRESS
const NETWORK = process.env.NETWORK
const RECEIVER = "0x5da29a0e4Dd6662aaC1a7CC18F09dB2E202002D9" //"0x63Bfb8478683632BBc7A85Cb85cbA6C89CF9DAbc";
const TylipaContract = require("../client/src/contracts/Tylipa.json");


if (!MNEMONIC || !INFURA_KEY || !OWNER_ADDRESS || !NETWORK) {
    console.error("Please set a mnemonic, infura key, owner, network, and contract address.")
    return
}

const NFT_ABI = require('../client/src/contracts/Tylipa.json')

async function main() {
    console.log('get wallet');
    const provider = new HDWalletProvider(MNEMONIC, `https://${NETWORK}.infura.io/v3/${INFURA_KEY}`)
    console.log('get web3 instance');
    const web3Instance = new web3(
        provider
    )

    console.log(await web3Instance.eth.net.getId());

    const networkId = await web3Instance.eth.net.getId();
    const networkNames = {
      "1" : "main",
      "3" : "ropsten",
      "4" : "rinkeby",
      "5" : "goerli",
      "42" : "kovan",
      "5777": "private",
    }
    const networkType = networkNames[networkId];

    console.log(networkType);

    const deployedNetwork = TylipaContract.networks[networkId];

    console.log('get contract');
    if ( deployedNetwork ) {
      const contract = new web3Instance.eth.Contract(
        TylipaContract.abi,
        deployedNetwork && deployedNetwork.address,
      );

      console.log(deployedNetwork.address);



      console.log('now mint to ' + RECEIVER);
      const result = await contract.methods.mintTo(RECEIVER).send({ from: OWNER_ADDRESS });
      console.log("Minted a flower to " + RECEIVER + " - Transaction: " + result.transactionHash)

    }
}

main()
