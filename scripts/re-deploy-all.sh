#!/bin/sh

rm -rf client/src/contracts/*
truffle migrate --network rinkeby

cd client
npm run build && npm run deploy
cd ..
